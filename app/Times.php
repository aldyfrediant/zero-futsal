<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Times extends Model
{
    protected $fillable = [
			'start',
			'finish'
		];



	public $timesteamps = false;

	protected $table = 'times';
}
