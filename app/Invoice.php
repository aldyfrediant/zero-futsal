<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
			'kode_booking',
			'total'
		];



	public $timesteamps = false;

	protected $table = 'invoices';
}
