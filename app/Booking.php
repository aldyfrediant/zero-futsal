<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	protected $fillable = [
			'kode_booking',
			'player_name',
			'contact',
			'date'
		];



	public $timesteamps = false;

	protected $table = 'bookings';
}
