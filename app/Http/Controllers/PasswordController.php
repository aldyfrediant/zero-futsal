<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use User;

class PasswordController extends Controller
{   
    public function __construct()
    {
        $this->pageName = 'Profile';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('admin.profile')
            ->with('user', $user);
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'newpassword => confirmed',
            'newpassword_confirmation',
        ]);       
        
        $request->user()->fill([
            'password' => Hash::make($request->newpassword)
        ])->save();

        return redirect('admin/profile')
            ->with('status', 'Sukses');      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
