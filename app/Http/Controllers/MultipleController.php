<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\Booking;
use App\Times;
use App\Invoice;
use Carbon\Carbon;

class MultipleController extends Controller
{
    private $pageName;

    public function __construct()
    {
        $this->pageName = 'Multiple Booking';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.booking.multiple.reservee')
            ->with('page', $this->pageName);
    }

    public function selectTime(Request $request)
    {
        $times = Times::all();
        $t = array();
        $i = 0;
        foreach($times as $data)
        {
            $t[1+$i] = $data->start.'.00 - '.$data->finish.'.00';
            $i++;
        }

        return view('admin.booking.multiple.time')
            ->with('name', $request->name)
            ->with('contact', $request->contact)
            ->with('t', $t)
            ->with('field', $request->field)
            ->with('page', $this->pageName);
    }

    public function submitBook(Request $request)
    {
        $max = count($request->date);
        $field = $request->field;

        $this->validate($request, [
            'name' => 'required',
            'contact' => 'required'
        ]);
        

        $mDate = array();
        $mStart = array();
        $mFinish = array();
        $mBooking = array();

        // Pricing
        if($request->field == 1){
            $price = 140;
        } elseif($request->field == 2) {
            $price = 70;
        }
        
        // Invoice
        $inv = New Invoice;
        $cm = Carbon::now()->month;
        $cy = Carbon::now()->year;
        $invRule = '/'.'INV'.'/'.$cm.'/'.$cy;
        $c = Invoice::where('kode', 'like', '%'.$invRule.'%')->count();
        $q = $c+1;        
            if($c <= 9){
                $tempInv = '00'.$q.'/'.$request->field.$invRule;
            }
            elseif($c <= 99){
                $tempInv = '0'.$q.'/'.$request->field.$invRule;
            }
            else{
                $tempInv = $q.'/'.$request->field.$invRule;
            }       

        $inv->kode = $tempInv;

        $subtotal = $price * $max;
        $discount = $request->discount;
        $disc = $subtotal * ($discount / 100);

        $grandtotal = $subtotal - $disc;
        $totalformated = $grandtotal * 1000;
        $inv->total = 'Rp. '.$totalformated;
        $inv->nama = $request->name;
        $inv->payment = "Pending";
        $inv->save();

        // Booking
        for($i=0;$i<$max;$i++){
            $booking = New Booking;

            $time = Times::find($request->time[$i]);
            $s = $time->start;
            $f = $time->finish;
            $dates = $request->date[$i];
            $x = Carbon::createFromFormat("d/m/Y", $dates)->toDateTimeString();
            $d = date("dmY", strtotime($x));
            $date = date("Y-m-d", strtotime($x));

            try {                
                $bookRule = $request->field.'/'.$s.$f.'/'.'BO'.'/'.$d;
                $booking->kode_booking = $bookRule;
                $booking->player_name = $request->name;
                $booking->contact = $request->contact;
                $booking->date = $date;
                $booking->status = "Pending";
                $booking->invoice_id = $inv->id;
                $booking->save();
            }
            catch(\Exception $e) {
                $tempbook = Booking::where('invoice_id', $inv->id)->get();
                foreach($tempbook as $bookdata){
                    Booking::destroy($bookdata->id);
                }
                Invoice::destroy($inv->id);

                $times = Times::all();
                $t = array();
                $i = 0;

                foreach($times as $data)
                {
                    $t[1+$i] = $data->start.'.00 - '.$data->finish.'.00';
                    $i++;
                }

                $error = "Satu / lebih waktu booking sudah terisi, mohon pilih waktu lain.";
                return redirect('admin/multiple')
                    ->with('status', $error);
            }

            $mDate[$i] = date("d-m-Y", strtotime($x));
            $mDateFormated[$i] = $mDate[$i];
            $mStart[$i] = $s;
            $mFinish[$i] = $f;
            $mBooking[$i] = $booking;
        }


        return view('admin.booking.multiple.invoice')
            ->with('date', $mDateFormated)
            ->with('start', $mStart)
            ->with('finish', $mFinish)
            ->with('book', $mBooking)
            ->with('field', $field)
            ->with('price', $price)
            ->with('sub', $subtotal)
            ->with('disc', $discount)
            ->with('max', $max)
            ->with('inv', $inv);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
