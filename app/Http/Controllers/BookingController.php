<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\Booking;
use App\Times;
use App\Invoice;
use Carbon\Carbon;

class BookingController extends Controller
{
    private $pageName;

    public function __construct()
    {
        $this->pageName = 'Booking';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $dates = array();
        $i = 0;
        while($i < 28){
            $dates[$i] = Carbon::now('Asia/Jakarta')->addDays($i);
            $i++;
        }

        $times = Times::all();
        $booking = Booking::all();

        $booklist = array();
        foreach($booking as $data){
            $booklist[$data->kode_booking] = $data->player_name;
        }

        return view('admin.booking.list')
            ->with('page', $this->pageName)
            ->with('booklist', $booklist)
            ->with('times', $times)
            ->with('dates', $dates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // Variables
        $timeID = $request->times;
        $booking = New Booking;
        $time = Times::find($timeID);
        $s = $time->start;
        $f = $time->finish;
        $dates = $request->date;
        $d = date("dmY", strtotime($dates));
        $date = date("Y-m-d", strtotime($dates));

        $field = $request->field;

        $dateFormated = date("d-m-Y", strtotime($dates));

        // Validation
        $this->validate($request, [
            'name' => 'required',
            'contact' => 'required'
        ]);

        // Pricing
        if($request->field == 1){
            $price = 140;
        } elseif($request->field == 2) {
            $price = 70;
        }

        // Invoice
        $inv = New Invoice;
        $cm = Carbon::now()->month;
        $cy = Carbon::now()->year;
        $invRule = '/'.'INV'.'/'.$cm.'/'.$cy;
        $c = Invoice::where('kode', 'like', '%'.$invRule.'%')->count();
        $q = $c+1;        
            if($c <= 9){
                $tempInv = '00'.$q.'/'.$request->field.$invRule;
            }
            elseif($c <= 99){
                $tempInv = '0'.$q.'/'.$request->field.$invRule;
            }
            else{
                $tempInv = $q.'/'.$request->field.$invRule;
            }

            $inv->kode = $tempInv;
            $inv->nama = $request->name;
            $priceformated = $price * 1000;
            $inv->total = 'Rp. '.$priceformated;
            $inv->payment = "Pending";
            $inv->save();

        // Booking
        $bookRule = $request->field.'/'.$s.$f.'/'.'BO'.'/'.$d;
        $booking->kode_booking = $bookRule;
        $booking->player_name = $request->name;
        $booking->contact = $request->contact;
        $booking->date = $date;
        $booking->status = "Pending";
        $booking->invoice_id = $inv->id;
        $booking->save();

        return view('admin/booking/invoice')
            ->with('inv', $inv)
            ->with('book', $booking)
            ->with('field', $field)
            ->with('start', $s)
            ->with('finish', $f)
            ->with('date', $dateFormated)
            ->with('page', $this->pageName);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function book(Request $request)
    {
        $t = $_GET["times"];
        $f = $_GET["field"];

        $page = $this->pageName;

        $date = date("d - m - Y", strtotime($request->date));
        $times = Times::find($t);
        
        return view('admin.booking.form')
            ->with('date', $date)
            ->with('times', $times)
            ->with('field', $f)
            ->with('page', $page);
    }
}
