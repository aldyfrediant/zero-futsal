@extends('admin.layouts.app')

@section('content')
  <!-- Page -->
  <div class="page">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <!-- Panel Floating Labels -->
 			@if (session('status'))
			    <div class="alert alert-success">
			        <b>{{ session('status') }}</b>!!
			        Password Berhasil diganti!
			    </div>
			@endif
          <div class="panel">
            <div class="panel-body container-fluid">
            	{!! Form::model($user,[
		            'method' => 'patch',
		            'route' => ['profile.update', $user->id],
		            'class' => 'form-horizontal'
		         ]) !!}
	              	<div class="row">
	              		<div class="col-lg-12 col-sm-12">
	              			<h4 class="form-subtitle">Ganti Password</h4>
	              		</div>
	              		<div class="col-lg-6 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::password('newpassword', ['class' => 'form-control', 'required']) !!}
			                 {!! Form::label('newpassword', 'Password Baru', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-6 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::password('newpassword_confirmation', ['class' => 'form-control', 'required']) !!}
			                 {!! Form::label('newpassword_confirmation', 'Konfirmasi Password', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12 col-sm-12">
			            	<button class="btn btn-primary">Save</button>
			            </div>
	              	</div>
            	{!! Form::close() !!}
            </div>
          </div>
          <!-- End Panel Floating Labels -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection