@extends('admin.layouts.app')

@section('content')

  <script src="{{URL::to('assets/global/js/Plugin/responsive-tabs.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/closeable-tabs.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/tabs.js')}}"></script>
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Booking List</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item active">Bookings</li>
      </ol>
    </div>
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-xs-12 col-lg-12">
              <!-- Example Hover Table -->
              <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne"
                      aria-controls="exampleTabsOne" role="tab">Lapangan 1</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo"
                      aria-controls="exampleTabsTwo" role="tab">Lapangan 2</a></li>
                    </li>
                  </ul>
                  <div class="tab-content p-t-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">                      
                      <div class="example table-responsive">
                        <h3 class="table-title">Daftar Booking Lapangan 1</h3>
                        <table class="table table-hover booking-table">
                          <thead>
                            <tr>
                              <th rowspan="3" class="vertical-middle">Jam Main</th>
                            </tr>
                            <tr>
                              @for($d=0;$d<=6;$d++)
                                <th>{{date("l", strtotime($dates[$d]))}}</th>
                              @endfor
                            </tr>                            
                            @php $date = date("Y-m-d"); @endphp
                            <tr>
                              @for($d=0;$d<=6;$d++)
                                <th>{{date("d - m - Y", strtotime($dates[$d]))}}</th>
                              @endfor
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($times as $data)
                            <tr class="text-center">
                              <td>{{$data->start.'.00 - '.$data->finish.'.00'}}</td>
                              @for($i=0;$i<=6;$i++)
                                {!! Form::open(['route' => 'book']) !!}
                                {!! Form::hidden('date', $dates[$i], []) !!}
                                {!! Form::hidden('times', $data->id, []) !!}
                                {!! Form::hidden('field', '1', []) !!}
                                <td>
                                @php $d = date('dmY', strtotime($dates[$i])); @endphp
                                @if(isset($booklist['1/'.$data->start.$data->finish.'/BO/'.$d]))
                                    {{$booklist['1/'.$data->start.$data->finish.'/BO/'.$d]}}
                                @else 
                                  <a href="{{URL::to('admin/book?date='.$dates[$i].'&times='.$data->id.'&field=1')}}" class="btn btn-primary">Book</a>
                                @endif
                                </td>
                                {!! Form::close() !!}
                              @endfor
                            </tr>
                          @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">                     
                      <div class="example table-responsive">
                        <h3 class="table-title">Daftar Booking Lapangan 2</h3>
                        <table class="table table-hover booking-table">
                          <thead>
                            <tr>
                              <th rowspan="3" class="vertical-middle">Jam Main</th>
                            </tr>
                            <tr>
                              @for($d=0;$d<=6;$d++)
                                <th>{{date("l", strtotime($dates[$d]))}}</th>
                              @endfor
                            </tr>                            
                            @php $date = date("Y-m-d"); @endphp
                            <tr>
                              @for($d=0;$d<=6;$d++)
                                <th>{{date("d - m - Y", strtotime($dates[$d]))}}</th>
                              @endfor
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($times as $data)
                            <tr class="text-center">
                              <td>{{$data->start.'.00 - '.$data->finish.'.00'}}</td>
                              @for($i=0;$i<=6;$i++)
                                {!! Form::open(['route' => 'book']) !!}
                                {!! Form::hidden('date', $dates[$i], []) !!}
                                {!! Form::hidden('times', $data->id, []) !!}
                                {!! Form::hidden('field', '2', []) !!}
                                <td>
                                  @php $d = date('dmY', strtotime($dates[$i])); @endphp
                                  @if(isset($booklist['2/'.$data->start.$data->finish.'/BO/'.$d]))
                                    {{$booklist['2/'.$data->start.$data->finish.'/BO/'.$d]}}
                                  @else 
                                    <a href="{{URL::to('admin/book?date='.$dates[$i].'&times='.$data->id.'&field=2')}}" class="btn btn-primary">Book</a>
                                  @endif
                                </td>
                                {!! Form::close() !!}
                              @endfor
                            </tr>
                          @endforeach
                          </tbody>
                        </table>
                      </div>                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Example Hover Table -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection