@extends('admin.layouts.app')

@section('content')
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Tambah Booking</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{URL::to('admin/booking')}}">Bookings</a></li>
        <li class="breadcrumb-item active">Add</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <!-- Panel Floating Labels -->
          <div class="panel">
            <div class="panel-body container-fluid">
	            @if (session('status'))
				    <div class="alert alert-danger">
				        {{ session('status') }}
				        silahkan lihat jadwal dengan click <a href='{{URL::to('admin/booking')}}' target="_blank" class='btn btn-warning'>Disini</a>
				    </div>
				@endif
            	{!! Form::open(['autocomplete' => 'off', 'action' => 'MultipleController@selectTime']) !!}
	              	<div class="row">
	              		<div class="col-lg-12 col-sm-12">
	              			<h4 class="form-subtitle">Reservee's Data</h4>
	              		</div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('name', null, ['class' => 'form-control']) !!}
			                 {!! Form::label('name', 'Nama', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('contact', null, ['class' => 'form-control']) !!}
			                 {!! Form::label('contact', 'Kontak', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::select('field', ['1' => '1', '2' => '2'], 'Member', ['class' => 'form-control']) !!}
			                 {!! Form::label('field', 'Pilih Lapangan', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12 col-sm-12">
			            	<button class="btn btn-primary">Submit</button>
			            </div>
	              	</div>
            	{!! Form::close() !!}
            </div>
          </div>
          <!-- End Panel Floating Labels -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection