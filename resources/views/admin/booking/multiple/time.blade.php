@extends('admin.layouts.app')

@section('content')  
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
  <script src="{{URL::to('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script src="{{URL::to('assets/js/multiple.js')}}"></script>
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Tambah Booking</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{URL::to('admin/booking')}}">Bookings</a></li>
        <li class="breadcrumb-item active">Add</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="row">
            <div class="col-md-6">
             {!! Form::open(['action' => 'MultipleController@submitBook']) !!}
              <div class="panel">
                <div class="panel-body container-fluid">
                  <div class="col-md-12 col-sm-12">
                    <h4 class="form-subtitle">Pilih Waktu Booking</h4>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group form-material floating">
                      <div id="times">
                        <div class="row time-select">
                          <div class="col-md-6">                           
                            {!! Form::text('date[]', null, ['data-plugin' => 'datepicker', 'class' =>'form-control pickadate', 'placeholder' => 'Pick a Date']) !!}
                          </div>
                          <div class="col-md-6">
                            {!! Form::select('time[]', $t, 'Pilih Waktu', ['class' => 'form-control']) !!}
                          </div>
                        </div>
                      </div>
                      <div class="row time-select">
                        <div class="col-md-6">
                          <a href="#" id="add-row" class="btn btn-success">Tambah Row</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
            <!-- Panel Floating Labels -->
              <div class="panel">
                <div class="panel-body container-fluid">
                    <div class="row">
                      <div class="col-md-12 col-sm-12">
                        <h4 class="form-subtitle">Reservee's Data</h4>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group form-material floating" data-plugin="formMaterial">
                          <div class="row">
                            <div class="col-md-3"><p>Nama</p></div>
                            <div class="col-md-1"><p>:</p></div>
                            <div class="col-md-7"><p>{{$name}}</p></div>
                          </div>
                          <div class="row">
                            <div class="col-md-3"><p>Kontak</p></div>
                            <div class="col-md-1"><p>:</p></div>
                            <div class="col-md-7"><p>{{$contact}}</p></div>
                          </div>
                          <div class="row">                          
                            <div class="col-md-3"><p>Lapangan</p></div>
                            <div class="col-md-1"><p>:</p></div>
                            <div class="col-md-7"><p>{{$field}}</p></div>
                          </div>
                          {!! Form::hidden('name', $name, []) !!}
                          {!! Form::hidden('contact', $contact, []) !!}
                          {!! Form::hidden('field', $field, []) !!}
                        </div>
                      </div>
                      <div class="col-md-4">                        
                        <div class="input-group input-group-icon">
                            {!! Form::text('discount', null, ['class' => 'form-control', 'placeholder' => 'Discount']) !!}
                            <span class="input-group-addon">
                              <span>%</span>
                            </span>
                          </div>
                      </div>
                      <div class="offset-md-5 col-md-3">                        
                          <button type="submit" class="btn btn-primary">Submit Booking</button>
                      </div>
                    </div>
                </div>
              </div>
              <!-- End Panel Floating Labels -->
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
  <script>
    $('.pickadate').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '0',
    orientation: 'bottom auto'
    });

    var c = 1;
    $("#add-row").click(function ($e) {
      $e.preventDefault();
      $("#times").append('<div id="timerow-'+c+'" class="row time-select"><div class="col-md-6">{!! Form::text('date[]', null, ['data-plugin' => 'datepicker', 'class' =>'form-control pickadate', 'placeholder' => 'Pick a Date']) !!}</div><div class="col-md-6">{!! Form::select('time[]', $t, 'Pilih Waktu', ['class' => 'form-control', 'required']) !!}</div></div>');
        c++;
      $('.pickadate').datepicker({
      format: 'dd/mm/yyyy',
      startDate: '0',
      orientation: 'bottom auto'
      });
    });
  </script>
@endsection