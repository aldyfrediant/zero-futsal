@extends('admin.layouts.app')

@section('content')
	<div class="page">
		<div class="page-content">
	      <div class="example-wrap">
	        <div class="row">	         
	           <div class="col-md-6 col-sm-12 col-xs-12">
	            <div class="card">
	              <div class="card-header card-header-transparent card-header-bordered">
	                Data Pemesan
	              </div>
	              <div class="card-block">
	                <h4 class="card-title">{{ $inv->kode }}</h4>
	                <p class="card-text">
	                	<div class="row">
		                	<div class="col-md-3">Nama</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$book[0]->player_name}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Kontak</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$book[0]->contact}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Lapangan</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$field}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Harga</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">Rp. {{$price}}.000,- x {{$max}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Sub-Total</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">Rp. {{$sub}}.000,-</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Discount</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$disc}}%</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Total Tagihan</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$inv->total}}</div>
		                </div>
	                </p>
	                <a href="{{URL::to('admin/invoices')}}" class="btn btn-primary">Daftar Invoice</a>
	              </div>
	            </div>
	           </div>
	          <div class="col-md-6 col-sm-12 col-xs-12">
	            <div class="card">
	              <div class="card-header card-header-transparent card-header-bordered">
	                Jadwal Dibooking
	              </div>
	              <div class="card-block">
	                <h4 class="card-title">Tanggal / Jam / Kode Booking</h4>
	                <p class="card-text">
	                	@php $i = 0; $c = 1;@endphp
                		@foreach($date as $d)
                			<div class="row">
			                	<div class="col-md-1">{{$c}}.</div>
			                	<div class="col-md-5">{{$d.' / '.$start[$i].' - '.$finish[$i]}}</div>
			                	<div class="col-md-1">Kode: </div>
			                	<div class="col-md-5">{{$book[$i]->kode_booking}}</div>
	                		</div>
	                	@php $i++; $c++; @endphp
	                	@endforeach
	                </p>
	              </div>
	            </div>
	           </div>
	        </div>
	      </div>
	      <!-- Example Card Content -->
      </div>
    </div>
@endsection