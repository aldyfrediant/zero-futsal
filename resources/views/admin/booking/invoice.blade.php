@extends('admin.layouts.app')

@section('content')
	<div class="page">
		<div class="page-content">
	      <div class="example-wrap">
	        <div class="row">
	          <div class="col-md-6 offset-md-3 col-sm-12 col-xs-12">
	            <div class="card">
	              <div class="card-header card-header-transparent card-header-bordered">
	                Invoice
	              </div>
	              <div class="card-block">
	                <h4 class="card-title">{{ $inv->kode }}</h4>
	                <p class="card-text">
	                	<div class="row">
		                	<div class="col-md-3">Kode Booking</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$book->kode_booking}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Nama</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$book->player_name}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Kontak</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$book->contact}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Jadwal</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$date.' / '.$start.' - '.$finish}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Lapangan</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$field}}</div>
		                </div>
	                	<div class="row">
		                	<div class="col-md-3">Total Tagihan</div>
		                	<div class="col-md-1">:</div>
		                	<div class="col-md-7">{{$inv->total}}</div>
		                </div>
	                </p>
	                <a href="{{URL::to('admin/invoices')}}" class="btn btn-primary">Daftar Invoice</a>
	              </div>
	            </div>
	           </div>
	        </div>
	      </div>
	      <!-- Example Card Content -->
      </div>
    </div>
@endsection