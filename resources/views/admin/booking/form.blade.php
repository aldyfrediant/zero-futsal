@extends('admin.layouts.app')

@section('content')
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Tambah Booking</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{URL::to('admin/booking')}}">Bookings</a></li>
        <li class="breadcrumb-item active">Add</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <!-- Panel Floating Labels -->
          <div class="panel">
            <div class="panel-body container-fluid">
            	{!! Form::open(['route' => 'booking.store', 'autocomplete' => 'off']) !!}
	              	<div class="row">
	              		<div class="col-lg-12 col-sm-12">
	              			<h4 class="form-subtitle">Reservee's Data</h4>
	              		</div>
			            {!! Form::hidden('times', $_GET["times"], []) !!}
	              		<div class="col-lg-6 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('name', null, ['class' => 'form-control']) !!}
			                 {!! Form::label('name', 'Nama', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-6 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('contact', null, ['class' => 'form-control']) !!}
			                 {!! Form::label('contact', 'Kontak', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12 col-sm-12">
	              			<h4 class="form-subtitle">Booking Details</h4>
	              		</div>
	              		<div class="col-lg-6 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                {!! Form::text('date', $date, ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('date', 'Date', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-2 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                {!! Form::text('ts', $times->start.'.00', ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('ts', 'Start', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-2 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                {!! Form::text('tf', $times->finish.'.00', ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('tf', 'Finish', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-2 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                {!! Form::text('field', $field, ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('field', 'Lapangan', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12 col-sm-12">
			            	<button class="btn btn-primary">Submit</button>
			            </div>
	              	</div>
            	{!! Form::close() !!}
            </div>
          </div>
          <!-- End Panel Floating Labels -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection