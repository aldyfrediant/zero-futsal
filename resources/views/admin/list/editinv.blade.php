@extends('admin.layouts.app')

@section('content')
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Tambah Booking</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item ">List</li>
        <li class="breadcrumb-item"><a href="{{URL::to('admin/invoices')}}">Invoice</a></li>
        <li class="breadcrumb-item active">Edit</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <!-- Panel Floating Labels -->
          <div class="panel">
            <div class="panel-body container-fluid">
            	{!! Form::model($inv,[
		            'method' => 'patch',
		            'route' => ['invoices.update', $inv->id]
	            ])!!}
	              	<div class="row">
	              		<div class="col-lg-12 col-sm-12">
	              			<h4 class="form-subtitle">Invoice</h4>
	              		</div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('kode', $inv->kode, ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('kode', 'Kode', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('name', $inv->nama, ['class' => 'form-control']) !!}
			                 {!! Form::label('name', 'Nama', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
	              		<div class="col-lg-4 col-sm-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                 {!! Form::text('total', $inv->total, ['class' => 'form-control', 'readonly']) !!}
			                 {!! Form::label('total', 'Total', ['class' => 'floating-label']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12">
			                <div class="form-group form-material floating" data-plugin="formMaterial">
			                  <label class="form-control-label" for="select">Pembayaran</label>
			                  {!! Form::select('payment', ['Pending' => 'Pending', 'Cash' => 'Cash', 'Transfer' => 'Transfer'], $inv->payment, ['class' => 'form-control']) !!}
			                </div>
			            </div>
			            <div class="col-lg-12 col-sm-12">
			            	<button class="btn btn-primary" type="submit">Save Data</button>
			            </div>
	              	</div>
            	{!! Form::close() !!}
            </div>
          </div>
          <!-- End Panel Floating Labels -->
        </div>        
        <div class="col-md-6 col-xs-12">
          <!-- Panel Floating Labels -->
          <div class="panel">
            <div class="panel-body container-fluid">
              	<div class="row">
              		<div class="col-lg-12 col-sm-12">
              			<h4 class="form-subtitle">Booking</h4>
              		</div>
              		@foreach($booking as $data)
              			<div class="row">
				            <div class="col-md-2">Kode</div>
				            <div class="col-md-1">:</div>
				            <div class="col-md-4">{{$data->kode_booking}}</div>
			            </div>
            		@endforeach
              	</div>
            </div>	
          </div>
          <!-- End Panel Floating Labels -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
@endsection