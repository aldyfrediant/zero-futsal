@extends('admin.layouts.app')

@section('content')
<link rel="stylesheet" href="{{URL::asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css')}}">
<script src="{{URL::asset('assets/global/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js')}}"></script>
<!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Booking List</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}">Home</a></li>
        <li class="breadcrumb-item">List</li>
        <li class="breadcrumb-item active">Booking</li>
      </ol>
    </div>
    <div class="page-content">
      <!-- Panel -->
      <div class="panel">
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-xs-12 col-lg-12">
              	<!-- Example Hover Table -->
              	<div class="example-wrap">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">                      
                      	<div class="example table-responsive">
                        	<table class="table table-hover booking-list" id="dataTable">
	                          	<thead>
	                          		<tr>
	                          			<th>Kode Booking</th>
	                          			<th>Nama Pemesan</th>
	                          			<th>Kontak</th>
	                          			<th>Status</th>
	                          			<th>Action</th>
	                          		</tr>
                          		</thead>
                          		<tbody>
                          			@foreach($booking as $data)
                          			<tr>
                          				<td>{{$data->kode_booking}}</td>
                          				<td>{{$data->player_name}}</td>
                          				<td>{{$data->contact}}</td>
                          				<td>{{$data->status}}</td>
                          				<td>
                          					<a href="{{URL::to('admin/booking-list/'.$data->id.'/approve')}}" class="btn btn-xs btn-icon btn-success @if($data->status == "Approved"){{"disabled"}}@endif"><i class="icon fa-check"></i></a>
                          					{{-- <a href="#" class="btn btn-xs btn-icon btn-warning"><i class="icon fa-edit"></i></a> --}}
                          					<a href="#" class="btn btn-xs btn-icon btn-danger @if($data->status == "Approved"){{"disabled"}}@endif"><i class="icon fa-times"></i></a>
                          				</td>
                          			</tr>
                          			@endforeach
                    			</tbody>
                			</table>
                		</div>
                	</div>
            	</div><!-- End Example Hover Table -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
  <script>
  	$(document).ready(function(){
	    $('#dataTable').DataTable();
	})
  </script>
@endsection