<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Zero Futsal | Just Play and Fun!</title>
  <link rel="apple-touch-icon" href="{{URL::to('assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{URL::to('assets/images/favicon.ico')}}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{URL::to('assets/global/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/css/bootstrap-extend.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/css/site.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/css/custom.css')}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/jquery-mmenu/jquery-mmenu.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/jquery-mmenu/jquery-mmenu.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/chartist/chartist.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/jvectormap/jquery-jvectormap.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/examples/css/dashboard/v1.css')}}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{URL::to('assets/global/fonts/font-awesome/font-awesome.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/fonts/material-design/material-design.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('assets/global/fonts/brand-icons/brand-icons.min.css')}}">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Arimo" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="{{URL::to('assets/global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{URL::to('assets/global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{URL::to('assets/global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->
  <!-- Page -->  
  <script src="{{URL::to('assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/jquery/jquery.js')}}"></script>
  <!-- Scripts -->
  <script src="{{URL::to('assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition site-navbar-small dashboard">

  @include('admin.layouts.header')
  @include('admin.layouts.sidebar')
  
  @yield('content')

  @include('admin.layouts.footer')

  <!-- Core  -->
  <script src="{{URL::to('assets/global/vendor/tether/tether.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/animsition/animsition.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/waves/waves.js')}}"></script>
  <!-- Plugins -->
  <script src="{{URL::to('assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/intro-js/intro.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/chartist/chartist.min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/peity/jquery.peity.min.js')}}"></script>
  <script src="{{URL::to('assets/global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
  <!-- Scripts -->
  <script src="{{URL::to('assets/global/js/State.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Component.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Base.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Config.js')}}"></script>
  <script src="{{URL::to('assets/js/Section/Menubar.js')}}"></script>
  <script src="{{URL::to('assets/js/Section/Sidebar.js')}}"></script>
  <script src="{{URL::to('assets/js/Section/PageAside.js')}}"></script>
  <script src="{{URL::to('assets/js/Section/GridMenu.js')}}"></script>
  <!-- Config -->
  <script src="{{URL::to('assets/global/js/config/colors.js')}}"></script>
  <script src="{{URL::to('assets/js/config/tour.js')}}"></script>
  <!-- Page -->
  <script src="{{URL::to('assets/js/Site.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/switchery.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/matchheight.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/jvectormap.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/peity.js')}}"></script>
  <script src="{{URL::to('assets/examples/js/dashboard/v1.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
  <script src="{{URL::to('assets/global/js/Plugin/material.js')}}"></script>
</body>
</html>