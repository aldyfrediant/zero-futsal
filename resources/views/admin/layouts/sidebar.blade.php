@if(isset($page))
  @php $page = $page; @endphp
@else
  @php $page = 'Page Name'; @endphp
@endif
  <div class="site-menubar">
    <ul class="site-menu">
      <li class="site-menu-item @if($page == 'Dashboard'){{'active'}}@endif">
        <a class="animsition-link" href="{{URL::to('admin')}}">
          <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
          <span class="site-menu-title">Dashboard</span>
        </a>
      </li>
      <li class="site-menu-item has-sub @if($page == 'Booking'){{'active'}}@endif">
        <a href="javascript:void(0)">
          <i class="site-menu-icon md-view-compact" aria-hidden="true"></i>
          <span class="site-menu-title">Add Booking</span>
          <span class="site-menu-arrow"></span>
        </a>
        <ul class="site-menu-sub">
          <li class="site-menu-item">
            <a class="animsition-link" href="{{URL::to('admin/booking')}}">
              <span class="site-menu-title">Single</span>
            </a>
          </li>
          <li class="site-menu-item">
            <a class="animsition-link" href="{{URL::to('admin/multiple')}}">
              <span class="site-menu-title">Multiple</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="site-menu-item @if($page == 'Daftar Booking'){{'active'}}@endif">
        <a class="animsition-link" href="{{URL::to('admin/booking-list')}}">
          <i class="site-menu-icon md-book" aria-hidden="true"></i>
          <span class="site-menu-title">Bookings List</span>
        </a>
      </li>
      <li class="site-menu-item @if($page == 'Invoice'){{'active'}}@endif">
        <a class="animsition-link" href="{{URL::to('admin/invoices')}}">
          <i class="site-menu-icon md-book" aria-hidden="true"></i>
          <span class="site-menu-title">Invoices List</span>
        </a>
      </li>{{-- 
      <li class="site-menu-item @if($page == 'Report'){{'active'}}@endif">
        <a class="animsition-link" href="{{URL::to('admin/report')}}">
          <i class="site-menu-icon md-collection-text" aria-hidden="true"></i>
          <span class="site-menu-title">Report</span>
        </a>
      </li> --}}
    </ul>
  </div>