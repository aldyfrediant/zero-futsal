@extends('front.layouts.app')

@section('content')
	<div id="main-container">
		<div class="container">
        <div class="row" style="margin-bottom: 50px;">
          <div class="col-md-6">
            <h2>LAPANG I</h2>
          </div>
          <div class="col-md-6" style="text-align: right;">
            <a href="#" class="btn btn-primary">&laquo; Prev Week</a>
            <a href="#" class="btn btn-danger">Change Field <i class="fa fa-refresh"></i></a>
            <a href="#" class="btn btn-primary">Next Week &raquo; </a>
          </div>
        </div>
        @if($field == 1)            
        <div class="example table-responsive">
          <table class="table table-hover booking">
            <thead>
              <tr>
                <th rowspan="3" class="vertical-middle">Jam Main</th>
              </tr>
              <tr>
                @for($d=0;$d<=6;$d++)
                      <th>{{date("l", strtotime($dates[$d]))}}</th>
                @endfor
              </tr>                            
              @php $date = date("Y-m-d"); @endphp
              <tr>
                @for($d=0;$d<=6;$d++)
                  <th>{{date("d - m - Y", strtotime($dates[$d]))}}</th>
                @endfor
              </tr>
            </thead>
            <tbody>
            @foreach($times as $data)
              <tr class="text-center">
                <td>{{$data->start.'.00 - '.$data->finish.'.00'}}</td>
                @for($i=0;$i<=6;$i++)
                  {!! Form::open(['route' => 'cust.book']) !!}
                  {!! Form::hidden('date', $dates[$i], []) !!}
                  {!! Form::hidden('times', $data->id, []) !!}
                  {!! Form::hidden('field', '1', []) !!}
                  <td>
                    @php $daterow = date("d", strtotime($dates[$i])); @endphp
                    @php $d = date('dmY', strtotime($dates[$i])); @endphp
                    @if($daterow == $day and $data->start <= $hour+1)
                        <span>Expired</span>
                    @else
                        @if(isset($booklist['1/'.$data->start.$data->finish.'/BO/'.$d]))
                          <a href="#" class="btn btn-primary disabled">Booked</a>
                        @else 
                          <a href="{{URL::to('book?date='.$dates[$i].'&times='.$data->id.'&field=1')}}" class="btn btn-primary">Book</a>
                        @endif
                    @endif
                  </td>
                  {!! Form::close() !!}
                @endfor
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        @elseif($field == 2)                  
        <div class="example table-responsive">
          <table class="table table-hover booking">
            <thead>
              <tr>
                <th rowspan="3" class="vertical-middle">Jam Main</th>
              </tr>
              <tr>
                @for($d=0;$d<=6;$d++)
                      <th>{{date("l", strtotime($dates[$d]))}}</th>
                @endfor
              </tr>                            
              @php $date = date("Y-m-d"); @endphp
              <tr>
                @for($d=0;$d<=6;$d++)
                  <th>{{date("d - m - Y", strtotime($dates[$d]))}}</th>
                @endfor
            </tr>
            </thead>
            <tbody>
            @foreach($times as $data)
              <tr class="text-center">
                <td>{{$data->start.'.00 - '.$data->finish.'.00'}}</td>
                @for($i=0;$i<=6;$i++)
                  {!! Form::open(['route' => 'cust.book']) !!}
                  {!! Form::hidden('date', $dates[$i], []) !!}
                  {!! Form::hidden('times', $data->id, []) !!}
                  {!! Form::hidden('field', '2', []) !!}
                  <td>
                    @php $daterow = date("d", strtotime($dates[$i])); @endphp
                    @php $d = date('dmY', strtotime($dates[$i])); @endphp
                    @if($daterow == $day and $data->start <= $hour+1)
                        <span>Expired</span>
                    @else
                        @if(isset($booklist['2/'.$data->start.$data->finish.'/BO/'.$d]))
                          <a href="#" class="btn btn-primary disabled">Booked</a>
                        @else 
                          <a href="{{URL::to('book?date='.$dates[$i].'&times='.$data->id.'&field=2')}}" class="btn btn-primary">Book</a>
                        @endif
                    @endif
                  </td>
                  {!! Form::close() !!}
                @endfor
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        @endif
		</div>
	</div>
@endsection