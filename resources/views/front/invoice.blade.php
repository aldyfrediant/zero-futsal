@extends('front.layouts.app')

@section('content')
	<div id="main-container">
		<div class="container">
			<div class="booking-form">
				<div class="row">
					<div class="col-md-12 form-title">Invoice</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">No. Invoice</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$inv->kode}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Kode Booking</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$book->kode_booking}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Nama</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$book->player_name}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Kontak</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$book->contact}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Jadwal</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$date.' / '.$start.$finish}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Lapangan</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$field}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Tagihan</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$inv->total}}</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<a href="{{URL::to('/')}}" class="btn btn-primary" style="margin-top: 20px;">Kembali ke Halaman Utama</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<span class="footnote">
							Lakukan pembayaran <b>MAXIMAL 1 JAM</b> setelah invoice ini dibuat. Pembayaran dapat dilakukan dengan cara cash /  transfer ke rekening <b>BCA : 09102390123 a/n Zero Futsal</b> lalu kirim bukti transfer ke whatsapp kami: <b>+62 811 22 923 76</b> disertakan dengan No. Invoice anda.
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection