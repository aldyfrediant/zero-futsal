@extends('front.layouts.app')

@section('content')
    <!-- Main Content -->
    <div id="main-container" style="padding: 80px 0;">
    	<div class="content">
        	<div class="container">
            	<div class="text-align-center">
                	<h2>Pilih Lapangan</h2>
                    <div class="spacer-40"></div>
                </div>
            	<div class="row">
                    <div class="col-lg-6">
                        <div class="job-block">
                            <a href="{{URL::to('reserve/1/1')}}" class="btn btn-default btn-dark btn-ghost pull-right btn-sm">Pilih Lapangan</a>
                            <div class="meta-data accent-color">Lapangan 1</div>
                            <h4>Rp. 140.000,- / Jam</h4>
                            <p>Memiliki Luas 100km<sup>2</sup> x 300ha, dengan mengunakan rumput sintetis blablabla</p>
                        </div>
                    </div>
                	<div class="col-lg-6">
                    	<div class="job-block">
                        	<a href="{{URL::to('reserve/2/1')}}" class="btn btn-default btn-dark btn-ghost pull-right btn-sm">Pilih Lapangan</a>
                        	<div class="meta-data accent-color">Lapangan 2</div>
                            <h4>Rp. 70.000,- / Jam</h4>
                            <p>Memiliki Luas 100km<sup>2</sup> x 300ha, dengan mengunakan rumput sintetis blablabla</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  	</div>
@endsection