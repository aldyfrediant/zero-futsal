@extends('front.layouts.app')

@section('content')
	<div id="main-container">
		<div class="container">
			<div class="booking-form">
				{!! Form::open(['route' => 'cust.store', 'autocomplete' => 'off']) !!}
				<div class="row">
					<div class="col-md-12 form-title">Detail Booking</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">Jam / Tanggal</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$times->start.'.00 - '.$times->finish.'.00 / '.$date}}</div>
				</div>
				<div class="row">
					<div class="col-md-2">Lapangan</div>
					<div class="col-md-1">:</div>
					<div class="col-md-9">{{$field}} ({{'Rp. '.$price.'.000 / Jam'}})</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12 form-title">Data Pemesan</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						{!! Form::label('name', "Nama", []) !!}
						{!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
					</div>
					<div class="col-md-6">						
						{!! Form::label('contact', "Kontak", []) !!}
						{!! Form::text('contact', null, ['class' => 'form-control', 'required']) !!}
					</div>
				</div>
				{!! Form::hidden('date', $_GET["date"], []) !!}
				{!! Form::hidden('field', $_GET["field"], []) !!}
				{!! Form::hidden('times', $_GET["times"], []) !!}
				<div class="row">
					<div class="col-md-3">
						<button class="btn btn-primary" type="submit">Submit Booking</button>
					</div>
				</div>
				<hr>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@endsection