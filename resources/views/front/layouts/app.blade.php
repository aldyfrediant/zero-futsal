<!DOCTYPE HTML>
<html class="no-js">
<head>
<!-- Basic Page Needs
  ================================================== -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Zero Futsal - Just Play and Fun!</title>
<meta name="description" content="Zero Futsal">
<meta name="keywords" content="Futsal">
<meta name="author" content="Pentacode Digital">
<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="format-detection" content="telephone=no">
<!-- CSS
  ================================================== -->
<link href="{{URL::asset('assets/front/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/css/bootstrap-theme.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/vendor/magnific/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/vendor/owl-carousel/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/vendor/owl-carousel/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="css/ie.css')}}" media="screen" /><![endif]-->
<!-- Color Style -->
<link href="{{URL::asset('assets/front/css/color3.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/front/css/custom.css')}}" rel="stylesheet" type="text/css"><!-- CUSTOM STYLESHEET FOR STYLING -->
<link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
<!-- SCRIPTS
  ================================================== -->
<script src="{{URL::asset('assets/front/js/modernizr.js')}}"></script><!-- Modernizr -->
<script src="{{URL::asset('assets/front/js/jquery-2.2.3.min.js')}}"></script> <!-- Jquery Library Call -->
</head>
<body class="home header-style1">
<!--[if lt IE 7]>
	<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->
<div class="body">

    @include('front.layouts.header')

    @yield('content')

    @include('front.layouts.footer')

</div>

<script src="{{URL::asset('assets/front/vendor/magnific/jquery.magnific-popup.min.js')}}"></script> <!-- Maginific Popup Plugin -->
<script src="{{URL::asset('assets/front/js/ui-plugins.js')}}"></script> <!-- UI Plugins -->
<script src="{{URL::asset('assets/front/js/helper-plugins.js')}}"></script> <!-- Helper Plugins -->
<script src="{{URL::asset('assets/front/js/bootstrap.js')}}"></script> <!-- UI -->
<script src="{{URL::asset('assets/front/js/init.js')}}"></script> <!-- All Scripts -->
<script src="{{URL::asset('assets/front/vendor/flexslider/js/jquery.flexslider.js')}}"></script> <!-- FlexSlider -->
</body>
</html>