    <div class="header-wrapper">
        <!-- Start Header -->
    	<div class="site-header">
        	<div class="container">
                <div class="site-logo">
                    <a href="index.html">
                        <img src="{{URL::to('assets/front/images/logo.png')}}" alt="" class="default-logo">
                        <img src="{{URL::to('assets/front/images/logo@2x.png')}}" alt="" class="retina-logo" width="164" height="45">
                    </a>
                </div>
                <a href="#" id="info-toggle"><i class="fa fa-bars"></i></a>
                <ul id="info-content">
                	<li>
                    	<i class="fa fa-phone"></i>
                    	<div class="ititle">Call us toll free</div>
                        <strong>1800-9090-8089</strong>
                    </li>
                	<li>
                    	<i class="fa fa-map-signs"></i>
                    	<div class="ititle">
                        <div class="galleryflex flexslider" data-direction="vertical" data-autoplay="yes" data-arrows="yes">
                        	<ul class="slides">
                            	<li class="slide">Suite 300 Houston<br>Texas - 77042 USA</li>
                            	<li class="slide">Level 2 Collins Road<br>Melbourne - Australia</li>
                            </ul>
                        </div>
                        
                        </div>
                    </li>
                	<li>
                    	<i class="fa fa-clock-o"></i>
                    	<div class="ititle">Working Hours</div>
                        <strong>Mon-Sat 09:00 to 17:00</strong>
                    </li>
                </ul>
                <a href="#" id="menu-toggle">Menu</a>
                <nav class="main-navigation pull-right">
                    <ul class="dd-menu sf-menu">
                        <li><a href="#">Book</a></li>
                    </ul>
                </nav>
          	</div>
        </div>
    </div>