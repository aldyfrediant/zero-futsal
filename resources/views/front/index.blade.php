@extends('front.layouts.app')

@section('content')
<script src="{{URL::asset('assets/front/vendor/owl-carousel/js/owl.carousel.min.js')}}"></script> <!-- Owl Carousel -->
    <!-- Start Hero Area -->
    <div class="hero-area">
    	<div class="flexslider heroflex hero-slider" data-autoplay="yes" data-pagination="no" data-arrows="yes" data-style="fade" data-pause="yes">
    	<div class="book-box">Booking Sekarang</div>
            <ul class="slides">
                <li class="parallax" style="background-image: url(http://placehold.it/1440x600&amp;text=IMAGE+PLACEHOLDER);">
                	<div class="flex-caption text-align-left vertical-center">
                    	<div class="container">
                        	<div class="flex-caption-table">
                            	<div class="flex-caption-cell">
                                	<div class="flex-caption-text">
                                        <h2>Protecting Public &amp; Private<br>lives for over 50 years <a href="#" class="ico"><i class="icon icon-music-play"></i></a></h2>
                        				<p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi.<br>Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum<br>Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada,</p>
                                    </div>
                               	</div>
                          	</div>
                        </div>
                    </div>
                </li>
                <li class="parallax" style="background-image: url(http://placehold.it/1440x600&amp;text=IMAGE+PLACEHOLDER);">
                	<div class="flex-caption text-align-center vertical-bottom">
                    	<div class="container">
                        	<div class="flex-caption-table">
                            	<div class="flex-caption-cell">
                                	<div class="flex-caption-text">
                                        <h2>Protecting Public &amp; Private lives for over 50 years</h2>
                                        <a href="book-service.html" class="btn btn-default btn-ghost btn-light btn-rounded">View some of our top cases</a>
                                    </div>
                               	</div>
                          	</div>
                        </div>
                    </div>
                </li>
          	</ul>
       	</div>
    </div>
    <!-- Main Content -->
    <div id="main-container">
    	<div class="content">
        	<div class="container">
              	<div class="text-align-center">
                	<h3>Fasilitas</h3>
                	<hr class="sm">
               	</div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="icon-box ibox-center ibox-plain">
                            <div class="ibox-icon margin-0">
                                <i class="icon icon-dialogue-happy"></i>
                            </div>
                            <h3>Amazing People</h3>
                            <p>Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="icon-box ibox-center ibox-plain">
                            <div class="ibox-icon margin-0">
                                <i class="icon icon-check"></i>
                            </div>
                            <h3>Experienced</h3>
                            <p>Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="icon-box ibox-center ibox-plain">
                            <div class="ibox-icon margin-0">
                                <i class="icon icon-diamond"></i>
                            </div>
                            <h3>Solid Foundation</h3>
                            <p>Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="icon-box ibox-center ibox-plain">
                            <div class="ibox-icon margin-0">
                                <i class="icon icon-music-random"></i>
                            </div>
                            <h3>Different Approach</h3>
                            <p>Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-60"></div>
            <section id="gallery">
	            <div class="carousel-wrapper">
	                <div class="row">
	                	<div class="gallery owl-carousel owl-theme">
                            <div class="item gallery-item"><img src="{{URL::to('assets/front/images/p1.jpg')}}"></div>
                            <div class="item gallery-item"><img src="{{URL::to('assets/front/images/p2.jpg')}}"></div>
                            <div class="item gallery-item"><img src="{{URL::to('assets/front/images/p3.jpg')}}"></div>
                            <div class="item gallery-item"><img src="{{URL::to('assets/front/images/p4.jpg')}}"></div>
						    <div class="item gallery-item"><img src="{{URL::to('assets/front/images/p5.jpg')}}"></div>
						</div>
	                </div>
	            </div>
            </section>
            <div class="spacer-60"></div>
            <section id="maps">
            	<div class="text-align-center">
            		<h3>Lokasi Kami</h3>
            		<hr class="sm">
            	</div>
    			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.647594124758!2d107.63366631533023!3d-6.932655994991078!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e870d1476019%3A0xfddadedf7d769e87!2sZero+Futsal!5e0!3m2!1sen!2sid!4v1502680983527" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    		</section>
        </div>
  	</div>

<script>
    $('.gallery').owlCarousel({
        autoplay:true,
        autoplayHoverPause:false,
        margin:0,
        loop:true,
        items:5
    })
</script>
@endsection