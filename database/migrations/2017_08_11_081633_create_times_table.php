<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('times', function (Blueprint $table) {
            $table->increments('id');
            $table->string('start');
            $table->string('finish');
            $table->timestamps();
        });

        DB::table('times')->insert([
        'start' => '07',
        'finish' => '08'
       ]);
        DB::table('times')->insert([
        'start' => '08',
        'finish' => '09'
       ]);
        DB::table('times')->insert([
        'start' => '09',
        'finish' => '10'
       ]);
        DB::table('times')->insert([
        'start' => '10',
        'finish' => '11'
       ]);
        DB::table('times')->insert([
        'start' => '11',
        'finish' => '12'
       ]);
        DB::table('times')->insert([
        'start' => '12',
        'finish' => '13'
       ]);
        DB::table('times')->insert([
        'start' => '13',
        'finish' => '14'
       ]);
        DB::table('times')->insert([
        'start' => '14',
        'finish' => '15'
       ]);
        DB::table('times')->insert([
        'start' => '15',
        'finish' => '16'
       ]);
        DB::table('times')->insert([
        'start' => '16',
        'finish' => '17'
       ]);
        DB::table('times')->insert([
        'start' => '17',
        'finish' => '18'
       ]);
        DB::table('times')->insert([
        'start' => '18',
        'finish' => '19'
       ]);
        DB::table('times')->insert([
        'start' => '19',
        'finish' => '20'
       ]);
        DB::table('times')->insert([
        'start' => '20',
        'finish' => '21'
       ]);
        DB::table('times')->insert([
        'start' => '21',
        'finish' => '22'
       ]);
        DB::table('times')->insert([
        'start' => '22',
        'finish' => '23'
       ]);
        DB::table('times')->insert([
        'start' => '23',
        'finish' => '24'
       ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('times');
    }
}
