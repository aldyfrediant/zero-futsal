<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front Page
Route::resource('/', 'FrontController');

// Customer Booking
	Route::get('reserve/field', 'CustomerBookingController@choosefield');
	Route::get('reserve/{field}/{week}', 'CustomerBookingController@index')->name('book.list');
	Route::get('book', 'CustomerBookingController@book')->name('cust.book');
	Route::post('/book', 'CustomerBookingController@store')->name('cust.store');

// Booking With Selected Date
	Route::get('reserve?date={date}&field={field}', 'CustomerBookingController@dateBook')->name('datebook');

// Admin Panel
Route::group(['prefix' => 'admin'], function () {

	// Dashboard Page
	Route::resource('/', 'DashboardController');

	// Single Booking Page
	Route::resource('/booking', 'BookingController');
		Route::get('book', 'BookingController@book')->name('book');
		Route::post('book?date={date}&times={id}', 'BookingController@bookSubmit')->name('book.submit');

	// Multiple Booking Page
	Route::resource('/multiple', 'MultipleController');
		Route::post('multiple/time', 'MultipleController@selectTime');
		Route::post('multiple/time/book', 'MultipleController@submitBook');

	// List
    Route::resource('/booking-list', 'ListBookingController');
		Route::get('/booking-list/{id}/approve', 'ListBookingController@approve')->name('book.approve');
    Route::resource('/invoices', 'InvoiceController');
    	Route::post('/invoices/{id}/edit', 'InvoiceController@approveBook');

	// Report Page
	Route::resource('/report', 'ReportController');

	Route::resource('/profile', 'PasswordController');
});

// Auth (No-Register)
Route::auth();
Route::get('logout', 'Auth\Logincontroller@logout');
Route::post('logout', 'Auth\LoginController@logout');
